package com.han.elections;

import com.han.elections.api.asynctask.UserAsyncTask;

import android.support.v7.app.ActionBarActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class LoginActivity extends ActionBarActivity {

	EditText edtPhoneNo;
	Button btnLogin;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_login);
		
		initWidget();
		initValue();
		initEvent();
	}
	
	private void initWidget() {
		edtPhoneNo = (EditText) findViewById(R.id.phone_no_editText);
		btnLogin = (Button) findViewById(R.id.submit_report_button);
	}
	
	private void initValue() {
		edtPhoneNo.setText("08061361064");
	}
	
	private void initEvent() {
		btnLogin.setOnClickListener(new Button.OnClickListener() {
			@Override
			public void onClick(View v) {
				new UserAsyncTask(LoginActivity.this).execute(UserAsyncTask.ACTION_LOGIN, edtPhoneNo.getText().toString());
			}
        });
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
	
	public void successLogin() {
		startActivity(new Intent(this, ReportActivity.class));
	}
}