package com.han.elections;

import com.han.elections.api.asynctask.UserAsyncTask;
import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class SubmitTNVActivity extends Activity {

	TextView txtElection;
	TextView txtBooth;
	
	EditText edtTotalVoters;
	Button btnSubmit;
	
	RelativeLayout rlytBack;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_submit_tnv);
		
		initWidget();
		initValue();
		initEvent();
	}
	
	private void initWidget() {
		txtElection = (TextView) findViewById(R.id.election_textView);
		txtBooth = (TextView) findViewById(R.id.booth_textView);
		
		edtTotalVoters = (EditText) findViewById(R.id.report_editText);
		btnSubmit = (Button) findViewById(R.id.send_button);
		
		rlytBack = (RelativeLayout) findViewById(R.id.back_home_relativeLayout);
	}
	
	private void initValue() {
		txtElection.setText(GlobalData.curUser.election.description);
		txtBooth.setText(GlobalData.curUser.booth.name);
	}
	
	private void initEvent() {
		btnSubmit.setOnClickListener(new Button.OnClickListener() {
			@Override
			public void onClick(View v) {
				String strCount = edtTotalVoters.getText().toString();
				new UserAsyncTask(SubmitTNVActivity.this).execute(UserAsyncTask.ACTION_SUBMIT_COUNT, strCount);
			}
        });
		rlytBack.setOnClickListener(new Button.OnClickListener() {
			@Override
			public void onClick(View v) {
				finish();
			}
        });
	}
	
	public void successSubmitCount() {
//		DialogUtility.show(this, "");
		finish();
	}
}
