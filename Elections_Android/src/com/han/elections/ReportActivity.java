package com.han.elections;

import java.net.URISyntaxException;

import com.han.elections.api.asynctask.UserAsyncTask;
import com.han.utility.DialogUtility;
import com.han.utility.FileUtility;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

public class ReportActivity extends ActionBarActivity {

	private static final int FILE_SELECT_CODE = 1001;
	
	TextView txtElection;
	TextView txtBooth;
	EditText edtReport;
	
	Button btnUpload;
	Button btnSend;
	
	LinearLayout llytSubmitTNV;
	LinearLayout llytSubmitResults;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_report);
		
		initWidget();
		initValue();
		initEvent();
	}
	
	private void initWidget() {
		txtElection = (TextView) findViewById(R.id.election_textView);
		txtBooth = (TextView) findViewById(R.id.booth_textView);
		
		edtReport = (EditText) findViewById(R.id.name_editText);
		
		llytSubmitTNV = (LinearLayout) findViewById(R.id.submit_tnv_linearLayout);
		llytSubmitResults = (LinearLayout) findViewById(R.id.submit_results_linearLayout);
		
		btnUpload = (Button) findViewById(R.id.upload_report_button);
		btnSend = (Button) findViewById(R.id.submit_report_button);
	}
	
	private void initValue() {
		txtElection.setText(GlobalData.curUser.election.description);
		txtBooth.setText(GlobalData.curUser.booth.name);
	}
	
	private void initEvent() {
		llytSubmitTNV.setOnClickListener(new Button.OnClickListener() {
			@Override
			public void onClick(View v) {
				startActivity(new Intent(ReportActivity.this, SubmitTNVActivity.class));
			}
        });
		llytSubmitResults.setOnClickListener(new Button.OnClickListener() {
			@Override
			public void onClick(View v) {
				startActivity(new Intent(ReportActivity.this, SubmitResultsActivity.class));
			}
        });
		btnSend.setOnClickListener(new Button.OnClickListener() {
			@Override
			public void onClick(View v) {
				String report = edtReport.getText().toString();
				if (report == null || report.equals("")) {
					DialogUtility.show(ReportActivity.this, report);
					return;
				}
				new UserAsyncTask(ReportActivity.this).execute(UserAsyncTask.ACTION_SUBMIT_REPORT, report);
			}
        });
		btnUpload.setOnClickListener(new Button.OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent intent = new Intent(Intent.ACTION_GET_CONTENT); 
			    intent.setType("*/*");
			    intent.addCategory(Intent.CATEGORY_OPENABLE);

			    try {
			        startActivityForResult(
			                Intent.createChooser(intent, "Select a File to Upload"),
			                FILE_SELECT_CODE);
			    } catch (android.content.ActivityNotFoundException ex) {
			        // Potentially direct the user to the Market with a Dialog
			    	DialogUtility.show(ReportActivity.this, "Please install a File Manager.");
			    }
			}
        });
	}
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
	    switch (requestCode) {
	        case FILE_SELECT_CODE:
	        if (resultCode == RESULT_OK) {
	            Uri uri = data.getData();
	            Log.e("File Uri", uri.toString());
				try {
					String path = FileUtility.getPath(this, uri);
		            Log.e("File Path", path);
				} catch (URISyntaxException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
	        }
	        
	        break;
	    }
	    super.onActivityResult(requestCode, resultCode, data);
	}
	
	public void successSubmitReport() {
		DialogUtility.show(this, "Report send successfully.");
	}
}
