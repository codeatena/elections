package com.han.elections.model;

import org.json.JSONException;
import org.json.JSONObject;

public class Party {

	public int id = 0;
	public String name = "";
	public String abbrev = "";
	
	public Party parse(JSONObject jsonObj) {
		try {
			id = jsonObj.getInt("id");
			name = jsonObj.getString("name");
			abbrev = jsonObj.getString("abbrev");
			
			return this;
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
	}
}
