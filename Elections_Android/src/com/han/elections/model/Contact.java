package com.han.elections.model;

import org.json.JSONException;
import org.json.JSONObject;

public class Contact {
	
	public int id = 0;
	public String firstName = "";
	public String lastName = "";
	public String email = "";
	public String phoneNumber = "";
	public int contactType = 0;
	public int contactLevel = 0;
	public int electionID = 0;
	public String created = "";
	
	public Contact parse(JSONObject jsonObj) {
		try {
			id = jsonObj.getInt("id");
			firstName = jsonObj.getString("first_name");
			lastName = jsonObj.getString("last_name");
			
			return this;
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
	}
}
