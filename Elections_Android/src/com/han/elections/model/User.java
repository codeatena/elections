package com.han.elections.model;

import java.util.ArrayList;
public class User {

	public Booth booth = new Booth();
	public Election election = new Election();
	public Contact contact = new Contact();
	
	public ArrayList<Party> arrParty = new ArrayList<Party>();
}
