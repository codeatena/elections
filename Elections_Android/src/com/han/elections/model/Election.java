package com.han.elections.model;

import org.json.JSONException;
import org.json.JSONObject;

public class Election {

	public int id = 0;
	public String description = "";
	
	public Election parse(JSONObject jsonObj) {
		try {
			id = jsonObj.getInt("id");
			description = jsonObj.getString("description");
			
			return this;
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
	}
}
