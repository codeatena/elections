package com.han.elections.model;

import org.json.JSONException;
import org.json.JSONObject;

public class Booth {
	public int id = 0;
	public String name = "";
	
	public Booth parse(JSONObject jsonObj) {
		try {
			
			if (!jsonObj.isNull("id") && !jsonObj.has("id")) {
				id = jsonObj.getInt("id");
			}
			if (!jsonObj.isNull("name") && !jsonObj.has("name")) {
				name = jsonObj.getString("name");
			}
			
			return this;
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
	}
}
