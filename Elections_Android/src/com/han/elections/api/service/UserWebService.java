package com.han.elections.api.service;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import com.han.elections.GlobalData;
import com.han.elections.api.MyWebService;
import com.han.elections.api.ParseResult;
import com.han.elections.api.parser.UserParser;

import android.util.Log;

public class UserWebService extends MyWebService {
	
	public static ParseResult login(String phone_number) {
		
		String url = URL_API + "/v1/agent-phone-pass";
		Log.e("url", url);
		Log.e("method", "login");
		
		List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
        nameValuePairs.add(new BasicNameValuePair("phone_number", phone_number));

		String strResponse = MyWebService.callHttpRequestGeneral(url, "POST", nameValuePairs);
		
		if (strResponse == null || strResponse.equals("")) {
			ParseResult pResult = new ParseResult(false, "failed call web service");
			return pResult;
		}
		
		return UserParser.loginParse(strResponse);
	}
	
	public static ParseResult getElection() {
		String url = URL_API + "/v1/enter-results?contact_id=" + Integer.toString(GlobalData.curUser.contact.id);
		
		Log.e("url", url);
		Log.e("method", "get_election");
		
		String strResponse = MyWebService.callHttpRequestGeneral(url, "GET", null);
		
		if (strResponse == null || strResponse.equals("")) {
			ParseResult pResult = new ParseResult(false, "failed call web service");
			return pResult;
		}
		
		return UserParser.parseGetElection(strResponse);
	}
	
	public static ParseResult submitCount(String strCount) {
		String url = URL_API + "/v1/total-voters-count";
		Log.e("url", url);
		Log.e("method", "submit_count");
		
		List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
		nameValuePairs.add(new BasicNameValuePair("contact_id", Integer.toString(GlobalData.curUser.contact.id)));
        nameValuePairs.add(new BasicNameValuePair("voters_count", strCount));
        
        String strResponse = MyWebService.callHttpRequestGeneral(url, "POST", nameValuePairs);
		
		if (strResponse == null || strResponse.equals("")) {
			ParseResult pResult = new ParseResult(false, "failed call web service");
			return pResult;
		}
		
		return UserParser.parseSubmitCount(strResponse);
	}
	
	public static ParseResult submitResults(String strVotes) {
		String url = URL_API + "/v1/enter-results";
		Log.e("url", url);
		Log.e("method", "submit_results");
		
		List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
		nameValuePairs.add(new BasicNameValuePair("contact_id", Integer.toString(GlobalData.curUser.contact.id)));
        nameValuePairs.add(new BasicNameValuePair("votes", strVotes));
        
        String strResponse = MyWebService.callHttpRequestGeneral(url, "POST", nameValuePairs);
		
		if (strResponse == null || strResponse.equals("")) {
			ParseResult pResult = new ParseResult(false, "failed call web service");
			return pResult;
		}
		
		return UserParser.parseSubmitResults(strResponse);
	}
	
	public static ParseResult submitReport(String report) {
		
		String url = URL_API + "/v1/submit-report";
		Log.e("url", url);
		Log.e("method", "submit_report");
		
		List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
		nameValuePairs.add(new BasicNameValuePair("contact_id", Integer.toString(GlobalData.curUser.contact.id)));
        nameValuePairs.add(new BasicNameValuePair("content", report));
        
        String strResponse = MyWebService.callHttpRequestGeneral(url, "POST", nameValuePairs);
		
		if (strResponse == null || strResponse.equals("")) {
			ParseResult pResult = new ParseResult(false, "failed call web service");
			return pResult;
		}
		
		return UserParser.parseSubmitReport(strResponse);
	}
}
