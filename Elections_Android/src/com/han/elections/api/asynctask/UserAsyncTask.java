package com.han.elections.api.asynctask;

import com.han.elections.LoginActivity;
import com.han.elections.ReportActivity;
import com.han.elections.SubmitResultsActivity;
import com.han.elections.SubmitTNVActivity;
import com.han.elections.api.MyAsyncTask;
import com.han.elections.api.ParseResult;
import com.han.elections.api.service.UserWebService;
import android.content.Context;

public class UserAsyncTask extends MyAsyncTask {

	public static String ACTION_LOGIN = "action_login";
	public static String ACTION_SUBMIT_COUNT = "action_submit_count";
	public static String ACTION_SUBMIT_RESULTS = "action_submit_results";
	public static String ACTION_SUBMIT_REPORT = "action_submit_report";

	public UserAsyncTask(Context _parent) {
		super(_parent);
		// TODO Auto-generated constructor stub
	}

	public UserAsyncTask(Context _parent, String _title) {
		super(_parent, _title);
		// TODO Auto-generated constructor stub
	}
	
	@Override
	protected ParseResult doInBackground(String... params) {
		// TODO Auto-generated method stub
		
		ParseResult pResult = new ParseResult(false);

		curAction = params[0];
		if (curAction.equals(ACTION_LOGIN)) {
			pResult = UserWebService.login(params[1]);
			if (pResult.isSuccess()) {
				pResult = UserWebService.getElection();
			}
		}
		if (curAction.equals(ACTION_SUBMIT_COUNT)) {
			pResult = UserWebService.submitCount(params[1]);
		}
		if (curAction.equals(ACTION_SUBMIT_RESULTS)) {
			pResult = UserWebService.submitResults(params[1]);
		}
		if (curAction.equals(ACTION_SUBMIT_REPORT)) {
			pResult = UserWebService.submitReport(params[1]);
		}

		return pResult;
	}
	
	protected void onPostExecute(ParseResult pResult) {
		
		super.onPostExecute(pResult);
		
		if (curAction.equals(ACTION_LOGIN)) {
			if (pResult.isSuccess()) {
				LoginActivity loginActivity = (LoginActivity) parent;
				loginActivity.successLogin();
			}
		}
		if (curAction.equals(ACTION_SUBMIT_COUNT)) {
			if (pResult.isSuccess()) {
				SubmitTNVActivity submitTNVActivity = (SubmitTNVActivity) parent;
				submitTNVActivity.successSubmitCount();
			}
		}
		if (curAction.equals(ACTION_SUBMIT_RESULTS)) {
			if (pResult.isSuccess()) {
				SubmitResultsActivity submitResultsActivity = (SubmitResultsActivity) parent;
				submitResultsActivity.successSubmitResults();
			}
		}
		if (curAction.equals(ACTION_SUBMIT_REPORT)) {
			if (pResult.isSuccess()) {
				ReportActivity reportActivity = (ReportActivity) parent;
				reportActivity.successSubmitReport();
			}
		}
	}
}
