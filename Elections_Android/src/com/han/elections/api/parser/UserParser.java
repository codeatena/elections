package com.han.elections.api.parser;
import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONObject;

import com.han.elections.GlobalData;
import com.han.elections.api.MyParser;
import com.han.elections.api.ParseResult;
import com.han.elections.model.Party;

public class UserParser extends MyParser {

	public static final int SUCCESS = 1;
	public static final int FAIL = -1;
	
	public static ParseResult loginParse(String strResponse) {
		try {
			JSONObject jsonObj = new JSONObject(strResponse);
			
			ParseResult pResult = new ParseResult();
//			pResult.result = jsonObj.getString("result");
//			pResult.message = jsonObj.getString("message");
			
			boolean isStatus = jsonObj.getBoolean("status");
			
			pResult.result = (isStatus) ? "success": "fail";
			if (pResult.isSuccess()) {
				JSONObject jsonContact = jsonObj.getJSONObject("contact");
				GlobalData.curUser.contact.parse(jsonContact);
			} else {
				pResult.message = jsonObj.getString("errors");
			}
			
			return pResult;
		} catch (Exception e) {
			e.printStackTrace();
			return new ParseResult(false, "json parsing error");
		}
	}
	
	public static ParseResult parseGetElection(String strResponse) {
		try {
			JSONObject jsonObj = new JSONObject(strResponse);
			
			ParseResult pResult = new ParseResult();

			boolean isStatus = jsonObj.getBoolean("status");
			
			pResult.result = (isStatus) ? "success": "fail";
			if (pResult.isSuccess()) {
				JSONArray jsonArrParties = jsonObj.getJSONArray("parties");
				GlobalData.curUser.arrParty = new ArrayList<Party>();
				for (int i = 0; i < jsonArrParties.length(); i++) {
					JSONObject jsonParty = jsonArrParties.getJSONObject(i);
					Party party = new Party().parse(jsonParty);
					if (party != null) {
						GlobalData.curUser.arrParty.add(party);
					}
				}
				
				GlobalData.curUser.booth.parse(jsonObj.getJSONObject("booth"));
				GlobalData.curUser.election.parse(jsonObj.getJSONObject("election"));
			} else {
				pResult.message = jsonObj.getString("errors");
			}
			
			return pResult;
		} catch (Exception e) {
			e.printStackTrace();
			return new ParseResult(false, "json parsing error");
		}
	}
	
	public static ParseResult parseSubmitCount(String strResponse) {
		try {
			JSONObject jsonObj = new JSONObject(strResponse);
			
			ParseResult pResult = new ParseResult();

			boolean isStatus = jsonObj.getBoolean("status");
			
			pResult.result = (isStatus) ? "success": "fail";
			if (pResult.isSuccess()) {
				
			} else {
				pResult.message = jsonObj.getString("errors");
			}
			
			return pResult;
		} catch (Exception e) {
			e.printStackTrace();
			return new ParseResult(false, "json parsing error");
		}
	}
	
	public static ParseResult parseSubmitResults(String strResponse) {
		try {
			JSONObject jsonObj = new JSONObject(strResponse);
			
			ParseResult pResult = new ParseResult();

			boolean isStatus = jsonObj.getBoolean("status");
			
			pResult.result = (isStatus) ? "success": "fail";
			if (pResult.isSuccess()) {
				pResult.message = jsonObj.getString("msg");
			} else {
				pResult.message = jsonObj.getString("errors");
			}
			
			return pResult;
		} catch (Exception e) {
			e.printStackTrace();
			return new ParseResult(false, "json parsing error");
		}
	}
	
	public static ParseResult parseSubmitReport(String strResponse) {
		try {
			JSONObject jsonObj = new JSONObject(strResponse);
			
			ParseResult pResult = new ParseResult();

			boolean isStatus = jsonObj.getBoolean("status");
			
			pResult.result = (isStatus) ? "success": "fail";
			if (pResult.isSuccess()) {
//				pResult.message = jsonObj.getString("msg");
			} else {
				pResult.message = jsonObj.getString("errors");
			}
			
			return pResult;
		} catch (Exception e) {
			e.printStackTrace();
			return new ParseResult(false, "json parsing error");
		}
	}
}
