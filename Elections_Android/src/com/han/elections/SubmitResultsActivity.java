package com.han.elections;

import org.json.JSONException;
import org.json.JSONObject;

import com.han.elections.api.asynctask.UserAsyncTask;
import com.han.elections.model.Party;
import com.han.utility.DialogUtility;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class SubmitResultsActivity extends Activity {

	TextView txtElection;
	TextView txtBooth;
	
	LinearLayout llytParties;
	RelativeLayout rlytBack;
	
	Button btnEnter;
	
	EditText[] edtNames;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_submit_results);
		
		initWidget();
		initValue();
		initEvent();
	}
	
	private void initWidget() {
		txtElection = (TextView) findViewById(R.id.election_textView);
		txtBooth = (TextView) findViewById(R.id.booth_textView);
		
		llytParties = (LinearLayout) findViewById(R.id.parties_linearLayout);
		rlytBack = (RelativeLayout) findViewById(R.id.back_home_relativeLayout);
		
		btnEnter = (Button) findViewById(R.id.submit_report_button);
	}
	
	@SuppressLint("InflateParams")
	private void initValue() {
		txtElection.setText(GlobalData.curUser.election.description);
		txtBooth.setText(GlobalData.curUser.booth.name);
		
		LayoutInflater inflater = LayoutInflater.from(this);

		edtNames = new EditText[GlobalData.curUser.arrParty.size()];
		
		int i = 0;
		for (Party party: GlobalData.curUser.arrParty) {
			View view = inflater.inflate(R.layout.row_election, null);
			TextView txtAbbrev = (TextView) view.findViewById(R.id.abbrev_textView);
			edtNames[i] = (EditText) view.findViewById(R.id.name_editText);
			
			txtAbbrev.setText(party.abbrev);
			edtNames[i].setText(party.name);
			edtNames[i].setId(party.id);
			
			llytParties.addView(view);
			i++;
		}
	}
	
	private void initEvent() {
		rlytBack.setOnClickListener(new Button.OnClickListener() {
			@Override
			public void onClick(View v) {
				finish();
			}
        });
		btnEnter.setOnClickListener(new Button.OnClickListener() {
			@Override
			public void onClick(View v) {
				JSONObject jsonObj = new JSONObject();
				for (int i = 0; i < edtNames.length; i++) {	
					try {
						jsonObj.put(Integer.toString(edtNames[i].getId()), edtNames[i].getText().toString());
					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
				
				Log.e("jsonObj", jsonObj.toString());
				
				new UserAsyncTask(SubmitResultsActivity.this).execute(UserAsyncTask.ACTION_SUBMIT_RESULTS, jsonObj.toString());
			}
        });
	}
	
	public void successSubmitResults() {
		DialogUtility.show(this, "Results entered successfully.");
		finish();
	}
}
